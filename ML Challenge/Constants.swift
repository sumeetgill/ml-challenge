//
//  Constants.swift
//  ML Challenge
//
//  Created by Sumeet Gill on 2018-02-05.
//  Copyright © 2018 Sumeet Gill. All rights reserved.
//
// I couldn't think of a creative name for this

import Foundation

class Constants {
    
    static let kAccountsSegue = "accountsSegue"
    static let kTransactionsSegue = "transactionsSegue"
    
    static let accountCell = "accountCell"
    static let transactionCell = "transactionCell"
    
    static let accountsJSONFile = "listOfAccounts"
    static let transactionJSONFile = "accountTransactions"
    
    static let isOpened = "isOpened"
}
