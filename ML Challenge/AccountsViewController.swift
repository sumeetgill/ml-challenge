//
//  AccountsViewController.swift
//  ML Challenge
//
//  Created by Sumeet Gill on 2018-02-04.
//  Copyright © 2018 Sumeet Gill. All rights reserved.
//
// FYI: My methods below use HeaderDoc, so you are able to option+click the method name to have it pop up

import UIKit

class AccountsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var accountsTableView: UITableView!
    
    var tableViewData = [Account]()
    
    // This will allow me to map the JSON file very easily, using a feature introduced in swift 4
    struct Account: Codable {
        let id: Int
        let display_name: String // Ideally this would be displayName, but I am using this to map simply
        let account_number: String
        let balance: Double
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true

        accountsTableView.delegate = self
        accountsTableView.dataSource = self
        
        if let account = retrieveAccountList() {
            tableViewData = account
        } else {
            // Add a dummy entry to array to display error
            tableViewData.append(Account(id: 0, display_name: "ERROR RETRIEVING ACCOUNTS", account_number: "", balance: 0.0))
        }
        
        accountsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /**
     Keeping track of if the user clicks open, so the correct page can be pushed
     
     - parameter sender: Quit button, which called this action
     */
    @IBAction func accountQuitButtonPressed(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: Constants.isOpened)
        navigationController?.popToRootViewController(animated: true)
    }

    /**
     Retrieves the account list via the JSON file on disk.
     This method will also parse the data into the Account object via the struct above
     
     - parameter N/A
     
     - returns: Optional Array of Account objects
     */
    func retrieveAccountList() -> [Account]? {
        if let filePath = Bundle.main.path(forResource: Constants.accountsJSONFile, ofType: "json") {
            if let jsonData: Data = try? Data.init(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe) {
                var accounts: [Account]? = nil
                
                do {
                    accounts = try JSONDecoder().decode([Account].self, from: jsonData)
                } catch {
                    NSLog("Error retrieving account list: \(error)")
                }
                
                return accounts
            }
        }
        
        return nil // something bad happened, return nothing
    }
    
    // MARK: - Tableview Delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: Constants.accountCell)
        let account = tableViewData[indexPath.row]
        var accountBalance = ""
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
    
        // Try to format in the user's preferred locale, if it fails it will be $xxxx
        if let balance = formatter.string(from: account.balance as NSNumber) {
            accountBalance = balance
        } else {
            accountBalance = "$" + String(account.balance)
        }
        
        cell.detailTextLabel?.numberOfLines = 0
        cell.textLabel?.text = "\(account.display_name) (\(account.account_number))"
        cell.detailTextLabel?.text = "Balance: \(accountBalance)"
        cell.tag = account.id
        cell.accessoryType = .detailDisclosureButton
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = accountsTableView.cellForRow(at: indexPath)
        accountsTableView.deselectRow(at: indexPath, animated: true)
        
        // prevent push if we failed to retrieve account info
        if cell?.tag != 0 {
            performSegue(withIdentifier: Constants.kTransactionsSegue, sender: cell?.tag)
        }
    }

    
    // MARK: - Navigation

    // Pass through the account number, so we know what to sort on the transactions page
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constants.kTransactionsSegue {
            let transactionVC:TransactionsViewController = segue.destination as! TransactionsViewController
            
            if let accountID:Int = sender as? Int {
                transactionVC.accountID = accountID
            } else {
                transactionVC.accountID = 000
            }
        }
    }
}
