//
//  TransactionsViewController.swift
//  ML Challenge
//
//  Created by Sumeet Gill on 2018-02-04.
//  Copyright © 2018 Sumeet Gill. All rights reserved.
//
// FYI: My methods below use HeaderDoc, so you are able to option+click the method name to have it pop up

import UIKit

class TransactionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var transactionsTableView: UITableView!
    
    var accountID: Int = 0
    var transactionData = [Transaction]()
    
    // Since the accountID is a dynamic key/value, I have to create a 'custom' object in order to loop through the values
    struct AccountList: Decodable {
        var accountID: [Int]
        var transactions: [Transaction]
        
        private struct CodingKeys: CodingKey {
            var intValue: Int?
            var stringValue: String
            
            init?(intValue: Int) {  self.intValue = intValue; self.stringValue = "" }
            init?(stringValue: String) { self.stringValue = stringValue }
        }
        
        init(from decoder: Decoder) throws {
            self.accountID = [Int]()
            self.transactions = [Transaction]()
            
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            for key in container.allKeys {
                
                // Although this is unnecessary since accountID should never be nil or break conversion, I'll add this for safety
                if let accountIDValue = Int(key.stringValue) {
                    self.accountID.append(accountIDValue)
                } else {
                    continue
                }
                
                try self.transactions = container.decode([Transaction].self, forKey: key)
            }
        }
    }
    
    struct Transaction: Codable {
        let date: String
        let activity: [Activity]
    }
    
    struct Activity: Codable {
        let id: Int
        let date: String
        let description: String
        let deposit_amount: Double? // Since it's one or the other, these have to be optionals
        let withdrawal_amount: Double?
        let balance: Double
        let transaction_uid: Int
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = false
        
        if let transactionList = retrieveTransactionList() {
            // using .filter I can make sure that I only get appropriate account data
            // using .flatmap I can 'flatten' an array, rather than loop through the array and append transaction to another array
            let filteredTransactionList:[AccountList] = transactionList.filter( { $0.accountID.contains(accountID) })
            let flatTransactionList:[Transaction] = filteredTransactionList.flatMap({ $0.transactions })
            
            transactionData = flatTransactionList
        }
        
        transactionsTableView.delegate = self
        transactionsTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     Formats the given amount in the user's preferred locale
     
     - parameter amount: Double that represents a currency amount
     
     - returns: String of formatted currency amount
     */
    func formatCurrency(amount:Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        var formattedAmount = ""
        
        if let formattedValue = formatter.string(from: amount as NSNumber) {
            formattedAmount = formattedValue
        } else {
            if amount > 0.0 {
                formattedAmount = "$" + String(amount)
            } else {
                formattedAmount = "-$" + String(amount)
            }
        }
        
        return formattedAmount
    }
    
    
    /**
     Retrieves the JSON data from disk and parses it using the codable structs listed above
     Method will fail safely in case object is modified or data is missing.
     
     - parameter N/A
     
     - returns: Optional Array of AccountList objects
     */
    func retrieveTransactionList() -> [AccountList]? {
        if let filePath = Bundle.main.path(forResource: Constants.transactionJSONFile, ofType: "json") {
            if let jsonData: Data = try? Data.init(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe) {
                var accounts: [AccountList]? = nil
                
                do {
                    accounts = try JSONDecoder().decode([AccountList].self, from: jsonData)
                } catch {
                    NSLog("Error retrieving transaction list: \(error)")
                }
                
                return accounts
            }
        }
        
        return nil // something bad happened, return nothing
    }

    
    // MARK: - Tableview Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return transactionData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionHeader = transactionData[section]
        
        return sectionHeader.date
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let  transaction = transactionData[section]
        
        return transaction.activity.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: Constants.transactionCell)
        
        let transaction = transactionData[indexPath.section]
        let activity = transaction.activity[indexPath.row]
        
        let balance = formatCurrency(amount: activity.balance)
        var transactionString = ""
        
        if let withdrawal = activity.withdrawal_amount {
            transactionString = "Withdrawal: (" + formatCurrency(amount: withdrawal) + ")"
        } else if let deposit = activity.deposit_amount {
            transactionString = "Deposit: " + formatCurrency(amount: deposit)
        }
        
        cell.textLabel?.text = activity.description
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = transactionString + "\n" + "Balance: \(balance)"

        return cell
    }
}
