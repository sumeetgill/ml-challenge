//
//  ViewController.swift
//  ML Challenge
//
//  Created by Sumeet Gill on 2018-02-04.
//  Copyright © 2018 Sumeet Gill. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBAction func openButtonPressed(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: Constants.isOpened)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let isOpened = UserDefaults.standard.bool(forKey: Constants.isOpened)
        
        if isOpened {
            self.performSegue(withIdentifier: Constants.kAccountsSegue, sender: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

