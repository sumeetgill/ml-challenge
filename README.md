# ML Challenge

### Summary

This is a take home project that I created for a job application. The app displays information that is parsed through a JSON file.

### How do I get set up?

Just pull and run. The project has no dependencies.


### Screenshots

![Account Page](https://i.imgur.com/5gzeN7r.jpg)![Transaction Page](https://i.imgur.com/8QbxZ9w.jpg)


### Acceptance Criteria
![acceptance](https://i.imgur.com/WlMvBPU.png)