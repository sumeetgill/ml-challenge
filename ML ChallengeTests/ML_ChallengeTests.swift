//
//  ML_ChallengeTests.swift
//  ML ChallengeTests
//
//  Created by Sumeet Gill on 2018-02-04.
//  Copyright © 2018 Sumeet Gill. All rights reserved.
//

import XCTest
@testable import ML_Challenge

class ML_ChallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFormatCurrency() {
        let transactionVC = TransactionsViewController()

        var testValue = transactionVC.formatCurrency(amount: 0.1234)
        XCTAssertEqual(testValue, "$0.12")
        
        testValue = transactionVC.formatCurrency(amount: 10.01)
        XCTAssertEqual(testValue, "$10.01")
        
        testValue = transactionVC.formatCurrency(amount: -111.119)
        XCTAssertEqual(testValue, "-$111.12")
    }
    
    func testRetrieveAccount() {
        let accountVC = AccountsViewController()
        
        var testValue = accountVC.retrieveAccountList()
        XCTAssertNotNil(testValue)
    }
    
    func testRetrieveTransaction() {
        let transactionVC = TransactionsViewController()

        let testValue = transactionVC.retrieveTransactionList()
        XCTAssertNotNil(testValue)
    }
}
